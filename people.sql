CREATE TABLE people(
        id int NOT NULL AUTO_INCREMENT,
        name varchar(30) NOT NULL,
        occupation varchar(45) NOT NULL,
        age int NOT NULL,
        PRIMARY KEY (id)
        );
        
INSERT INTO mystudentdb.people VALUES
(101, 'Peter', 'Content Writer', 32),
(102, 'Joseph', 'Developer', 30),
(103, 'Mike', 'Leader', 28),
(104, 'Stephen', 'Scientist', 45);

SELECT * FROM mystudentdb.people;
UPDATE mystudentdb.people 
SET name = 'Peter', occupation = 'Content Writer' 
WHERE id = 101;