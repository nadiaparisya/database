CREATE TABLE staff(
        staff_id varchar(5) NOT NULL AUTO_INCREMENT,
        staff_name varchar(30) NOT NULL,
        staff_age int NOT NULL,
        manager_id varchar(5) NOT NULL,
        PRIMARY KEY (staff_id)
        );