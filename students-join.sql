CREATE TABLE students(
        student_id int NOT NULL AUTO_INCREMENT,
        student_name varchar(30) NOT NULL,
        course_name varchar(30) NOT NULL,
        PRIMARY KEY (student_id)
        );

INSERT INTO officedb.students VALUES
(1, 'Aryan', 'Java'),
(2, 'Rohini', 'Hadoop'),
(3, 'Lallu', 'MongoDB');

SELECT * FROM officedb.students;

SELECT * FROM officedb.students;
SELECT * FROM officedb.officers;
SELECT officedb.officers.officer_name, officedb.officers.address, officedb.students.course_name
FROM officedb.officers
INNER JOIN officedb.students
ON officedb.officers.officer_id = officedb.students.student_id;

SELECT * FROM officedb.students;
SELECT * FROM officedb.officers;
SELECT officedb.students.student_id, officedb.students.student_name, officedb.officers.officer_id, officedb.officers.officer_name 
FROM officedb.students LEFT OUTER JOIN officedb.officers ON officedb.students.student_id = officedb.officers.officer_id;