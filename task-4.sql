CREATE TABLE manager(
        manager_id varchar(5) NOT NULL AUTO_INCREMENT,
        manager_name varchar(30) NOT NULL,
        dept_id varchar(5) NOT NULL,
        PRIMARY KEY (manager_id)
        );

INSERT INTO officedb.manager VALUE
('MA119', 'Lala', 'D1099'),
('MA112', 'Nana', 'D1098'),
('M404', 'Baba', 'D1097');

SELECT * FROM officedb.manager;

DELETE FROM officedb.manager WHERE dept_id = 'D1099';