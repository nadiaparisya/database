CREATE TABLE payment(
        payment_id int NOT NULL AUTO_INCREMENT,
        emp_id int NOT NULL,
        amount int NOT NULL,
        payment_date date NOT NULL,
        PRIMARY KEY (payment_id)
        );

INSERT INTO officedb.payment VALUES
(101, 101, 1200, '2015-09-15'),
(102, 101, 1200, '2010-09-30'),
(103, 103, 1500, '2005-10-15'),
(104, 103, 1500, '2018-09-30'),
(105, 102, 1800, '2010-09-30'),
(106, 102, 1800, '2005-10-12');

SELECT * FROM officedb.payment;