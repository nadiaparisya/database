CREATE TABLE trainer(
        id int NOT NULL AUTO_INCREMENT,
        course_name varchar(10) NOT NULL,
        trainer varchar(30) NOT NULL,
        email varchar (45) NOT NULL,
        PRIMARY KEY (id)
        );

INSERT INTO testdb.trainer VALUES
(001, 'Java', 'Mike', 'mike@javapoint.com'),
(002, 'Python', 'James', 'james@javapoint.com'),
(003, 'Android', 'Robin', 'robin@javapoint.com'),
(004, 'Hadoop', 'Stephen', 'stephen@javapoint.com'),
(005, 'Testing', 'Micheal', 'micheal@javapoint.com');

SELECT * FROM testdb.trainer;

UPDATE testdb.trainer   
SET email = 'mike@tutorialandexamples.com'   
WHERE course_name = 'Java';