CREATE TABLE employee(
        emp_id int NOT NULL AUTO_INCREMENT,
        name varchar(30) NOT NULL,
        birthdate date NOT NULL,
        gender varchar(2) NOT NULL,
        hire_date date NOT NULL,
        PRIMARY KEY (emp_id)
        );

INSERT INTO officedb.employee VALUES
(101, 'Bryan', '1988-08-12', 'M', '2015-08-26'),
(102, 'Joseph', '1984-05-12', 'M', '2014-10-21'),
(103, 'Mike', '1984-10-13', 'M', '2017-10-28'),
(104, 'Daren', '1979-04-11', 'M', '2006-11-01'),
(105, 'Marie', '1990-02-11', 'F', '2018-10-12'),
(106, 'Marco', '1988-04-11', 'M', '2010-10-12'),
(107, 'Antonio', '1982-02-15', 'M', '2005-10-12');

SELECT * FROM officedb.employee;
DELETE FROM employee WHERE emp_id=107;