CREATE TABLE employee_detail(
        id int NOT NULL AUTO_INCREMENT,
        name varchar(30) NOT NULL,
        email varchar(45) NOT NULL,
        phone int NOT NULL,
        city varchar(30) NOT NULL,
        working_hours int NOT NULL,
        PRIMARY KEY (id)
        );

INSERT INTO officedb.employee_detail VALUES
(1, 'Peter', 'peter@javatpoint.com', 62, 'Texas', 12),
(2, 'Suzi', 'suzi@javatpoint.com', 65, 'California', 10),
(3, 'Joseph', 'joseph@javatpoint.com', 64, 'Alaska', 14),
(4, 'Alex', 'alex@javatpoint.com', 61, 'Los Angeles', 9),
(5, 'Mark', 'mark@javatpoint.com', 60, 'Washington', 12),
(6, 'Stephen', 'stephen@javatpoint.com', 68, 'New York', 10);

SELECT * FROM officedb.employee_detail;
SELECT name FROM officedb.employee_detail;