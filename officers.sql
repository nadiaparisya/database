CREATE TABLE officers(
        officer_id int NOT NULL AUTO_INCREMENT,
        officer_name varchar(30) NOT NULL,
        address int NOT NULL,
        PRIMARY KEY (officer_id)
        );

INSERT INTO officedb.officers VALUES
(1, 'Aject', 'Mau'),
(2, 'Deepika', 'Lucknow'),
(3, 'Vimal', 'Faizabad'),
(4, 'Rahul', 'Lucknow');

SELECT * FROM officedb.officers;